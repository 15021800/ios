//
//  AppDelegate.h
//  QuoteQuiz
//
//  Created by admin on 9/6/16.
//  Copyright © 2016 NeoQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

