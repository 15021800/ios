//
//  ViewController.h
//  QuoteQuiz
//
//  Created by admin on 9/6/16.
//  Copyright © 2016 NeoQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizTipViewController.h"

@class Quiz;
@interface ViewController : UIViewController <QuizTipViewControllerDelegate>

@property (nonatomic, strong) Quiz *quiz;
@property (nonatomic, assign) NSInteger quizIndex;

- (IBAction)ans1Action:(id)sender;
- (IBAction)ans2Action:(id)sender;
- (IBAction)ans3Action:(id)sender;
- (IBAction)startAgain:(id)sender;

@end

