//
//  main.m
//  QuoteQuiz
//
//  Created by admin on 9/6/16.
//  Copyright © 2016 NeoQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
